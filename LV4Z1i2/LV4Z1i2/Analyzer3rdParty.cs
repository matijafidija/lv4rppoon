﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV4Z1i2
{
    class Analyzer3rdParty
    {
        public double[] PerRowAverage(double[][] data)
        {
            int rowCount = data.Length;
            double[] results = new double[rowCount];
            for (int i = 0; i < rowCount; i++)
            {
                results[i] = data[i].Average();
            }
            return results;
        }
        public double[] PerColumnAverage(double[][] data)
        {
            int redovi = 0, stupci = 0;
            foreach (double[] red in data)
            {
                foreach (double el in red)
                {
                    redovi++;
                }
                
                stupci++;
            }
            redovi = redovi / stupci;

            double[] results = new double[redovi];

            foreach (double[] red in data)
            {
                int i = 0;
                foreach (double broj in red)
                {
                    results[i] += broj;
                    i++;
                }
            }
            for (int i = 0; i < redovi; i++)
            {

                results[i] = results[i] / stupci;
            }
            
            return results;
        }
    }
}
