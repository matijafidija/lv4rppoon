﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV4Z1i2
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\data.csv";
            Dataset dataSet = new Dataset(path);
            Analyzer3rdParty service = new Analyzer3rdParty();
            Adapter adapter = new Adapter(service);
            double[] a = adapter.CalculateAveragePerColumn(dataSet);
            foreach (double br in a)
            {
                Console.WriteLine(br);
            }
        }
    }
}
