﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV4Z1i2
{
    class Adapter : IAnalytics
    {
        private Analyzer3rdParty analyticsService;
        public Adapter(Analyzer3rdParty service)
        {
            this.analyticsService = service;
        }


        private double[][] ConvertData(Dataset dataset)
        {
            int brRedova = 0;
            int brStupaca = 0;

            foreach (List<double> Row in dataset.GetData())
            {
                brRedova++;
            }

            double[][] matrica = new double[brRedova][];

            int i = 0, j = 0;

            foreach (List<double> Row in dataset.GetData())
            {
                foreach (double el in Row)
                {
                    brStupaca++;
                }
                matrica[i] = new double[brStupaca];
                foreach (double el in Row)
                {
                    matrica[i][j] = el;
                    Console.Write(matrica[i][j] + " ");
                    j++;
                }
                Console.Write("\n");
                brStupaca = 0;
                j = 0;
                i++;
            }

            return matrica;
        }

        public double[] CalculateAveragePerColumn(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerColumnAverage(data);
        }

        public double[] CalculateAveragePerRow(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerRowAverage(data);
        }

    }
}
